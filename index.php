<?php 

session_start();

include 'inc/config.php';

if(isset($_SESSION['users_email']) || isset($_COOKIE['users_email']) ){
    header("location:myaccount.php");
    exit();
}
    if(isset($_POST['login'])){
        
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        $statement = $conn->prepare("SELECT * FROM tbl_users WHERE users_email = ? AND users_password = ?");
        $statement->execute(array($email,$password));
        
        $num = $statement->rowCount();   
        
        
        
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        
        
        
        if($num>0){
           
            foreach($result as $row){
                
               
                
                
                
                if($row['status']==1){
                   
                    session_start();
                    
                    $_SESSION['users_email'] = $email;
                    
                    if(isset($_POST['remember_me'])){
                        
                        setcookie("users_email",$email,time()+60*5);
                        
                    }
                    
                    header("location:myaccount.php");
                    exit();
                }
                else{
                    header("location:index.php?err=".urlencode("The user account is not activated"));
                    exit();
                }
            }
        }
        else{
            header("location:index.php?err=".urlencode("Wrong Email or Password"));
            exit();
        }
        

        
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Starter Template for Bootstrap</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
       
        <link rel="stylesheet" href="css/style.css">
        

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Project name</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Login</a></li>
                        <li><a href="register.php">Register</a></li>
                        
                        
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">

            <form action="index.php" method="post" style="margin-top:35px">
               
                <h2>Login</h2>
                
                <?php 
                    if(isset($_GET['sucess'])){
                        ?>
                <div class="alert alert-success"><?php echo $_GET['sucess']; ?></div>
                        <?php
                    }
                ?>
                <?php 
                if(isset($_GET['err'])){
                ?>
                <div class="alert alert-danger"><?php echo $_GET['err']; ?></div>
                <?php
                }
                ?>
                
                <hr>
                
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember_me"> Check me out
                    </label>
                </div>
                <button type="submit" name="login" class="btn btn-default">Submit</button>
                <a href="forgot_password.php">Forgot Password?</a>
            </form>

        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    </body>
</html>
