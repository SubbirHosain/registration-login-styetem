<?php 



session_start();

include 'inc/config.php';

if(isset($_SESSION['users_email']) || isset($_COOKIE['users_email'])){
    header("location:myaccount.php");
    exit();
}



function isUnique($email){

    global $conn;
    
    $statement = $conn->prepare("SELECT * FROM tbl_users WHERE users_email=?");
    $statement->execute(array($email));			
    $num = $statement->rowCount();
    if($num>0){
        return false;
    }
    else{
        return true;
    }
    
}

if(isset($_POST['register'])){

    $_SESSION['name']=$_POST['name'];
    $_SESSION['email']=$_POST['email'];
    $_SESSION['password']=$_POST['password'];
    $_SESSION['confirm_password']=$_POST['confirm_password'];
        
    if(strlen($_POST['name'])<3){
        header("Location:register.php?err=" .urlencode("The name must be at least 3 characters longer"));
        exit();
    }
    else if($_POST['password']!=$_POST['confirm_password']){
        header("Location:register.php?err=" .urlencode("The Password and Confirm password do not match"));
        exit();
    }
    else if(strlen($_POST['password'])<5){
        header("Location:register.php?err=" .urlencode("The Password must be at least 5 characters"));
        exit();
    } 
    else if(strlen($_POST['confirm_password'])<5){
        header("Location:register.php?err=" .urlencode("The confirm Password must be at least 5 characters"));
        exit();
    }
    else if(!isUnique($_POST['email'])){
        header("Location:register.php?err=" .urlencode("Email is alreay in use. Please Try another one."));
        exit();
    }
    else{
        
        $name =$_POST['name'];
        $email =$_POST['email'];
        $password =$_POST['password'];
        $token = bin2hex(openssl_random_pseudo_bytes(32));
        
        $statement = $conn->prepare("INSERT INTO tbl_users (users_name,users_email,users_password,users_token) values(?,?,?,?)");
        $statement->execute(array($name,$email,$password,$token));
        
        $message = " Hi $name! Account created here is the activation link http://localhost/registration/active.php?token=$token";
        
        mail($email, 'Activate Account',$message,'From: subbir.ict@gmail.com');
        header('location:index.php?sucess='.urlencode("Activation Email Sent!")); 
        exit();

    }
    
}
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Starter Template for Bootstrap</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <link rel="stylesheet" href="css/style.css">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Project name</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li ><a href="index.php">Login</a></li>
                        <li class="active"><a href="register.php">Register</a></li>
                       

                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">

            <form action="register.php" method="POST" style="margin-top:35px">
                <h2>Register here</h2>
                
                <?php 
                
                    if(isset($_GET['err'])){
                        ?>
                        <div class="alert alert-danger"><?php echo $_GET['err'] ;?></div>
                        <?php
                    }    
                
                ?>
                
                <hr>
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" name="name" class="form-control" value="<?php echo @$_SESSION['name']; ?>" id="exampleInputEmail1" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" value="<?php echo @$_SESSION['email'];; ?>" id="exampleInputEmail1" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" value="<?php echo  @$_SESSION['password'];; ?>" id="exampleInputPassword1" placeholder="Password">
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Confirm Password</label>
                    <input type="password" name="confirm_password" class="form-control" value="<?php echo  @$_SESSION['confirm_password']; ?>" id="exampleInputPassword1" placeholder="Confirm Password">
                </div>

                <button type="submit" name="register" class="btn btn-default">Register</button>
            </form>

        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    </body>
</html>
