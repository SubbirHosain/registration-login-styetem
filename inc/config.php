<?php

//MySQL connection details.
$host = 'localhost';
$user = 'root';
$pass = 'root';
$database = 'registration';

//Custom PDO options.
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false
);
try {

    //Connect to MySQL and instantiate our PDO object.
    $conn = new PDO("mysql:host=$host;dbname=$database", $user, $pass, $options);	
    
   

}

catch(PDOException $e) {
    echo "Connection error: ".$e->getMessage();
}