<?php 

include 'inc/config.php';

if(isset($_POST['send_password'])){
    
    $email = $_POST['email'];
    
    $statement = $conn->prepare("SELECT * FROM tbl_users WHERE users_email=?");
    $statement->execute(array($email));
    $num = $statement->rowCount();  
    
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    
    if($num>0){
        
        foreach($result as $row){
           
            $password = $row['users_password'];
            
            if(mail($email,'Your Password!!',"Your pssword is : $password ","From: subbir.ict@gmail.com")){
                header("location:forgot_password.php?sucess=".urlencode("Your password has been sent to your email!!"));
                exit();
            }
            else{
                header("location:forgot_password.php?err=".urlencode("Sorry we could not send you password at this time!!"));
                exit(); 
            }
        }
    }
    
    else{
        header("location:forgot_password.php?err=".urlencode("Sorry there is no user with the provided email!!"));
        exit(); 
    }
    

    
}

?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Starter Template for Bootstrap</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <link rel="stylesheet" href="css/style.css">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Project name</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li ><a href="index.php">Login</a></li>
                        <li><a href="register.php">Register</a></li>

                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">

            <form action="forgot_password.php" method="post" style="margin-top:35px">
              
              <h2>Retrieve Password</h2>
              
                <?php 
                if(isset($_GET['sucess'])){
                ?>
                <div class="alert alert-success"><?php echo $_GET['sucess']; ?></div>
                <?php
                }
                ?>
                <?php 
                if(isset($_GET['err'])){
                ?>
                <div class="alert alert-danger"><?php echo $_GET['err']; ?></div>
                <?php
                }
                ?>
              
              <hr>
               
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                </div>



                <button type="submit" name="send_password" class="btn btn-default">Send My Password</button>
                <a href="index.php" class="btn btn-danger">Cancel</a>
                
            </form>

        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    </body>
</html>
